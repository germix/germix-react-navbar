import React, { PropsWithChildren } from 'react';
import { Menu, PopupWrapper } from '@germix/germix-react-core';
import NavbarButton from './NavbarButton';

interface Props extends PropsWithChildren<any>
{
    icon?,
    image?,
    disabled? : boolean,
    rounded?,
    roundedCircle?,
}
interface State
{
    menuOpen: boolean,
}

/**
 * @author Germán Martínez
 */
class NavbarMenuButton extends React.Component<Props, State>
{
    state: State =
    {
        menuOpen: false,
    }
    render()
    {
        return (
<>
<PopupWrapper>
    <NavbarButton
        icon={this.props.icon}
        image={this.props.image}
        active={this.state.menuOpen}
        disabled={this.props.disabled}
        rounded={this.props.rounded}
        roundedCircle={this.props.roundedCircle}
        onClick={() =>
        {
            this.setState({
                menuOpen: true
            });
        }}
    >
    </NavbarButton>

    {!this.state.menuOpen
        ?
        null
        :
        <Menu
            arrow
            onClose={ () =>
            {
                this.setState({
                    menuOpen: false
                });
            }}
        >
            { this.props.children }
        </Menu>
    }
</PopupWrapper>
</>
        );
    }
};
export default NavbarMenuButton;
