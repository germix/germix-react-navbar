import React from 'react';

interface Props
{
    theme?: 'dark'|'light';
    collapsing?: 'sm'|'md'|'lg'|'xl';
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class Navbar extends React.Component<Props,State>
{
    render()
    {
        const className = [
            'navbar',
            `navbar-expand-${this.props.collapsing || 'lg'}`,
        ];
        if(this.props.theme)
        {
            className.push(`navbar-theme-${this.props.theme}`);
        }
        return (
<div className={className.join(' ')}>
    { this.props.children }
</div>
        );
    }
}
export default Navbar;
