import React from 'react';

interface Props
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarText extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="navbar-text">
    { this.props.children }
</div>
        );
    }
}
export default NavbarText;
