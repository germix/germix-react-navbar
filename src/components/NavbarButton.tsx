import React from 'react';

interface Props
{
    icon?,
    image?,
    onClick(),
    active? : boolean,
    disabled? : boolean,
    rounded?,
    roundedCircle?,
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarButton extends React.Component<Props,State>
{
    render()
    {
        let className = "navbar-button";
        if(this.props.active)
        {
            className += ' active';
        }
        if(this.props.disabled)
        {
            className += ' disabled';
            className += ' strikethrough';
        }
        return (
<div className={className} onClick={() =>
{
    if(!(this.props.disabled))
    {
        this.props.onClick();
    }
}}>
    { this.props.icon &&
        <i className={this.props.icon}></i>
    }
    { this.props.image &&
        <img className={(() =>
        {
            let className = 'navbar-button-image';
            if(this.props.rounded)
                className += ' rounded';
            if(this.props.roundedCircle)
                className += ' rounded-circle';

            return className;
        })()} src={this.props.image}/>
    }
</div>
        );
    }
}
export default NavbarButton;
