import React from 'react';

interface Props
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarCenter extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="navbar-center">
    { this.props.children }
</div>
        );
    }
}
export default NavbarCenter;
