import React from 'react';

interface Props
{
    href,
    image,
    title?,
    onClick(e),
}
interface State
{
}

/**
 * @author Germán Martínez
 * 
 * @deprecated
 */
class NavbarLogo extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="navbar-logo">
    <a href={this.props.href} title={this.props.title} onClick={this.props.onClick}>
        <img src={this.props.image} title={this.props.title}/>
    </a>
</div>
        );
    }
}
export default NavbarLogo;
