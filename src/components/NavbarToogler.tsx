import React from 'react';

interface Props
{
    onToggle();
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarToogler extends React.Component<Props,State>
{
    render()
    {
        return (
<button
    className="navbar-toggler"
    type="button"
    data-toggle="collapse"
    aria-expanded="false"
    aria-label="Toggle navigation"
    onClick={() =>
    {
        this.props.onToggle();
    }}
>
    <span className='navbar-toggler-icon'/>
</button>
        );
    }
}
export default NavbarToogler;
