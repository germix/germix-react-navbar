import React from 'react';

interface Props
{
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarCollapse extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="navbar-collapse">
    { this.props.children }
</div>
        );
    }
}
export default NavbarCollapse;
