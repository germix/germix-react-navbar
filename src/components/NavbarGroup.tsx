import React from 'react';

interface Props
{
    grow?,
}
interface State
{
}

/**
 * @author Germán Martínez
 * 
 * @deprecated
 */
class NavbarGroup extends React.Component<Props,State>
{
    render()
    {
        return (
<div className="navbar-group" style={{
    flexGrow: this.props.grow
}}>
    { this.props.children }
</div>
        );
    }
}
export default NavbarGroup;
