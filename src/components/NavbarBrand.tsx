import React from 'react';

interface Props
{
    href?;
    title?;
    image?;
}
interface State
{
}

/**
 * @author Germán Martínez
 */
class NavbarBrand extends React.Component<Props,State>
{
    render()
    {
        return (
<a className="navbar-brand" href={this.props.href}>
    { this.props.image &&
        <img src={this.props.image} title={this.props.title} alt={this.props.title} className="d-inline-block align-top"/>
    }
    &nbsp;{ this.props.title }
</a>
        );
    }
}
export default NavbarBrand;
