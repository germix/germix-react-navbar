# Germix React Navbar

## About

Germix react navbar components

## Installation

```bash
npm install @germix/germix-react-navbar
```

## Build

```bash
npm run build
```

## Publish

```bash
npm publish
```

## Build & Publish

```bash
npm run build-publish
```
